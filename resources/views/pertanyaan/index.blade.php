@extends('layout.master')

@push('css')
<link href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
@endpush

@section('content')
	<div class="card">
      <div class="card-header">
        <h3 class="card-title">Table Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      	@if(session('success'))
      		<div class="alert alert-success">
      			{{session('success')}}
      		</div>
      	@endif
      	<div class="new">
      		<a href="pertanyaan/create" class="btn btn-primary mb-2">Create New Question</a>
      	</div>
        <table id="example1" class="table table-bordered table-striped">
          <thead>
	          <tr style="text-align:center">
	            <th>No</th>
	            <th>Judul</th>
	            <th>Isi</th>
	            <th>Action</th>
	          </tr>
          </thead>
          <tbody>
          	@forelse($pertanyaan as $key => $value)
          	  <tr>
          	  	<td style="text-align:center">{{$key + 1}}</td>
          	  	<td>{{$value->judul}}</td>
          	  	<td>{{$value->isi}}</td>
          	  	<td style="display:flex; text-align:center">
          	  		<a href="pertanyaan/{{$value->id}}" class="btn btn-info btn-sm">Details</a>
          	  		<a href="pertanyaan/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
          	  		<form action="/pertanyaan/{{$value->id}}" method="post">
          	  			@csrf
                		@method('DELETE')
          	  			<input type="submit" value="Delete" class="btn btn-danger btn-sm">
          	  		</form>
          	  	</td>
          	  </tr>
          	@empty
          	  <tr>
          	  	<td colspan="4" align="center">No Data</td>
          	  </tr>
          	@endforelse
        </tbody>
      </table>
    </div>
   </div>
@endsection

@push('script')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
@endpush